/*
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (C) 2021 Raspberry Pi Ltd
 */

import QtQuick 2.15
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.2
import QtQuick.Window 2.15
import "qmlcomponents"

Window {
    id: popup
    width: 430
    maximumWidth: 430
    minimumWidth: 430
    minimumHeight: 125
    height: Math.min(750, cl.implicitHeight)
    title: qsTr("OS Customization")

    property bool initialized: false
    property bool hasSavedSettings: false
    property string config
    property string cmdline
    property string firstrun
    property string cloudinit
    property string cloudinitrun
    property string cloudinitwrite
    property string cloudinitnetwork

    signal saveSettingsSignal(var settings)

    ColumnLayout {
        id: cl
        spacing: 10
        anchors.fill: parent

        // Keys handlers can only be attached to Items. Window is not an
        // Item, but ColumnLayout is, so put this handler here.
        Keys.onEscapePressed: {
            popup.close()
        }

        ScrollView {
            id: popupbody
            font.family: roboto.name
            //Layout.maximumWidth: popup.width-30
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.leftMargin: 25
            Layout.topMargin: 10
            clip: true
            //ScrollBar.vertical.policy: ScrollBar.AlwaysOn

            ColumnLayout {
                TabBar {
                    id: bar
                    Layout.fillWidth: true

                    TabButton {
                        text: qsTr("General")
                    }
                    TabButton {
                        text: qsTr("Options")
                    }
                }

                GroupBox {
                    StackLayout {
                        width: parent.width
                        currentIndex: bar.currentIndex

                        ColumnLayout {
                            // General tab
                            spacing: -10

                            RowLayout {
                                ImCheckBox {
                                    id: chkHostname
                                    text: qsTr("Set hostname:")
                                    onCheckedChanged: {
                                        if (checked) {
                                            fieldHostname.forceActiveFocus()
                                        }
                                    }
                                }
                                TextField {
                                    id: fieldHostname
                                    enabled: chkHostname.checked
                                    text: "citadel"
                                    selectByMouse: true
                                    validator: RegularExpressionValidator { regularExpression: /[0-9A-Za-z][0-9A-Za-z-]{0,62}/ }
                                }
                                Text {
                                    text : ".local"
                                    color: chkHostname.checked ? "black" : "grey"
                                }
                            }

                            ImCheckBox {
                                id: chkWifi
                                text: qsTr("Configure wireless LAN")
                                onCheckedChanged: {
                                    if (checked) {
                                        if (!fieldWifiSSID.length) {
                                            fieldWifiSSID.forceActiveFocus()
                                        } else if (!fieldWifiPassword.length) {
                                            fieldWifiPassword.forceActiveFocus()
                                        }
                                    }
                                }
                            }
                            GridLayout {
                                enabled: chkWifi.checked
                                Layout.leftMargin: 40
                                columns: 2
                                columnSpacing: 10
                                rowSpacing: -5

                                Text {
                                    text: qsTr("SSID:")
                                    color: parent.enabled ? (fieldWifiSSID.indicateError ? "red" : "black") : "grey"
                                }
                                TextField {
                                    id: fieldWifiSSID
                                    Layout.minimumWidth: 200
                                    selectByMouse: true
                                    property bool indicateError: false
                                    onTextEdited: {
                                        indicateError = false
                                    }
                                }

                                Text {
                                    text: qsTr("Password:")
                                    color: parent.enabled ? (fieldWifiPassword.indicateError ? "red" : "black") : "grey"
                                }
                                TextField {
                                    id: fieldWifiPassword
                                    Layout.minimumWidth: 200
                                    selectByMouse: true
                                    echoMode: chkShowPassword.checked ? TextInput.Normal : TextInput.Password
                                    property bool indicateError: false
                                    onTextEdited: {
                                        indicateError = false
                                    }
                                }

                                RowLayout {
                                    Layout.columnSpan: 2

                                    ImCheckBox {
                                        id: chkShowPassword
                                        text: qsTr("Show password")
                                        checked: true
                                    }
                                    ImCheckBox {
                                        id: chkWifiSSIDHidden
                                        Layout.columnSpan: 2
                                        text: qsTr("Hidden SSID")
                                        checked: false
                                    }
                                }

                                Text {
                                    text: qsTr("Wireless LAN country:")
                                    color: parent.enabled ? "black" : "grey"
                                }
                                ComboBox {
                                    id: fieldWifiCountry
                                    editable: true
                                }
                            }
                        }


                        ColumnLayout {
                            // Options tab
                            spacing: -10

                            ImCheckBox {
                                id: chkBeep
                                text: qsTr("Play sound when finished")
                            }
                            ImCheckBox {
                                id: chkEject
                                text: qsTr("Eject media when finished")
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter | Qt.AlignBottom
            Layout.bottomMargin: 10
            spacing: 20

            ImButtonBlue {
                text: qsTr("SAVE")
                onClicked: {
                    if (chkWifi.checked)
                    {
                        // Valid Wi-Fi PSKs are:
                        // - 0 characters (indicating an open network)
                        // - 8-63 characters (passphrase)
                        // - 64 characters (hashed passphrase, as hex)
                        if (fieldWifiPassword.text.length > 0 &&
                            (fieldWifiPassword.text.length < 8 || fieldWifiPassword.text.length > 64))
                        {
                            fieldWifiPassword.indicateError = true
                            fieldWifiPassword.forceActiveFocus()
                        }
                        if (fieldWifiSSID.text.length == 0)
                        {
                            fieldWifiSSID.indicateError = true
                            fieldWifiSSID.forceActiveFocus()
                        }
                        if (fieldWifiSSID.indicateError || fieldWifiPassword.indicateError)
                        {
                            bar.currentIndex = 0
                            return
                        }
                    }

                    applySettings()
                    saveSettings()
                    popup.close()
                }
            }

            Text { text: " " }
        }
    }

    function initialize() {
        chkBeep.checked = imageWriter.getBoolSetting("beep")
        chkEject.checked = imageWriter.getBoolSetting("eject")
        var settings = imageWriter.getSavedCustomizationSettings()
        fieldWifiCountry.model = imageWriter.getCountryList()

        if (Object.keys(settings).length) {
            hasSavedSettings = true
        }
        if ('hostname' in settings) {
            fieldHostname.text = settings.hostname
            chkHostname.checked = true
        }

        if ('wifiSSID' in settings) {
            fieldWifiSSID.text = settings.wifiSSID
            if ('wifiSSIDHidden' in settings && settings.wifiSSIDHidden) {
                chkWifiSSIDHidden.checked = true
            }
            chkShowPassword.checked = false
            fieldWifiPassword.text = settings.wifiPassword
            fieldWifiCountry.currentIndex = fieldWifiCountry.find(settings.wifiCountry)
            if (fieldWifiCountry.currentIndex == -1) {
                fieldWifiCountry.editText = settings.wifiCountry
            }
            chkWifi.checked = true
        } else {
            fieldWifiCountry.currentIndex = fieldWifiCountry.find("GB")
            fieldWifiSSID.text = imageWriter.getSSID()
            if (fieldWifiSSID.text.length) {
                fieldWifiPassword.text = imageWriter.getPSK()
                if (fieldWifiPassword.text.length) {
                    chkShowPassword.checked = false
                    if (Qt.platform.os == "osx") {
                        /* User indicated wifi must be prefilled */
                        chkWifi.checked = true
                    }
                }
            }
        }

        if (imageWriter.isEmbeddedMode()) {
            /* For some reason there is no password mask character set by default on Embedded edition */
            var bulletCharacter = String.fromCharCode(0x2022);
            fieldUserPassword.passwordCharacter = bulletCharacter;
            fieldWifiPassword.passwordCharacter = bulletCharacter;
        }

        initialized = true
    }

    function openPopup() {
        if (!initialized) {
            initialize()
            if (imageWriter.hasSavedCustomizationSettings())
            {
                applySettings()
            }
        }

        //open()
        show()
        raise()
        popupbody.forceActiveFocus()
    }

    function addCmdline(s) {
        cmdline += " "+s
    }
    function addConfig(s) {
        config += s+"\n"
    }
    function addFirstRun(s) {
        firstrun += s+"\n"
    }
    function escapeshellarg(arg) {
        return "'"+arg.replace(/'/g, "'\"'\"'")+"'"
    }
    function addCloudInit(s) {
        cloudinit += s+"\n"
    }
    function addCloudInitWriteFile(name, content, perms) {
        cloudinitwrite += "- encoding: b64\n"
        cloudinitwrite += "  content: "+Qt.btoa(content)+"\n"
        cloudinitwrite += "  owner: root:root\n"
        cloudinitwrite += "  path: "+name+"\n"
        cloudinitwrite += "  permissions: '"+perms+"'\n"
    }
    function addCloudInitRun(cmd) {
        cloudinitrun += "- "+cmd+"\n"
    }

    function applySettings()
    {
        cmdline = ""
        config = ""
        firstrun = ""
        cloudinit = ""
        cloudinitrun = ""
        cloudinitwrite = ""
        cloudinitnetwork = ""

        if (chkHostname.checked && fieldHostname.length) {
            addFirstRun("CURRENT_HOSTNAME=`cat /etc/hostname | tr -d \" \\t\\n\\r\"`")
            addFirstRun("if [ -f /usr/lib/raspberrypi-sys-mods/imager_custom ]; then")
            addFirstRun("   /usr/lib/raspberrypi-sys-mods/imager_custom set_hostname "+fieldHostname.text)
            addFirstRun("else")
            addFirstRun("   echo "+fieldHostname.text+" >/etc/hostname")
            addFirstRun("   sed -i \"s/127.0.1.1.*$CURRENT_HOSTNAME/127.0.1.1\\t"+fieldHostname.text+"/g\" /etc/hosts")
            addFirstRun("fi")

            addCloudInit("hostname: "+fieldHostname.text)
            addCloudInit("manage_etc_hosts: true")
            addCloudInit("packages:")
            addCloudInit("- avahi-daemon")
            /* Disable date/time checks in apt as NTP may not have synchronized yet when installing packages */
            addCloudInit("apt:")
            addCloudInit("  conf: |")
            addCloudInit("    Acquire {")
            addCloudInit("      Check-Date \"false\";")
            addCloudInit("    };")
            addCloudInit("")
        }

        if (chkWifi.checked) {
            var wpaconfig = "country="+fieldWifiCountry.editText+"\n"
            wpaconfig += "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n"
            wpaconfig += "ap_scan=1\n\n"
            wpaconfig += "update_config=1\n"
            wpaconfig += "network={\n"
            if (chkWifiSSIDHidden.checked) {
                wpaconfig += "\tscan_ssid=1\n"
            }
            wpaconfig += "\tssid=\""+fieldWifiSSID.text+"\"\n"

            const isPassphrase = fieldWifiPassword.text.length >= 8 &&
                fieldWifiPassword.text.length < 64
            var cryptedPsk = isPassphrase ? imageWriter.pbkdf2(fieldWifiPassword.text, fieldWifiSSID.text)
                                          : fieldWifiPassword.text
            wpaconfig += "\tpsk="+cryptedPsk+"\n"
            wpaconfig += "}\n"

            addFirstRun("if [ -f /usr/lib/raspberrypi-sys-mods/imager_custom ]; then")
            addFirstRun("   /usr/lib/raspberrypi-sys-mods/imager_custom set_wlan "
                        +(chkWifiSSIDHidden.checked ? " -h " : "")
                        +escapeshellarg(fieldWifiSSID.text)+" "+escapeshellarg(cryptedPsk)+" "+escapeshellarg(fieldWifiCountry.editText))
            addFirstRun("else")
            addFirstRun("cat >/etc/wpa_supplicant/wpa_supplicant.conf <<'WPAEOF'")
            addFirstRun(wpaconfig)
            addFirstRun("WPAEOF")
            addFirstRun("   chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf")
            addFirstRun("   rfkill unblock wifi")
            addFirstRun("   for filename in /var/lib/systemd/rfkill/*:wlan ; do")
            addFirstRun("       echo 0 > $filename")
            addFirstRun("   done")
            addFirstRun("fi")


            cloudinitnetwork  = "version: 2\n"
            cloudinitnetwork += "wifis:\n"
            cloudinitnetwork += "  renderer: networkd\n"
            cloudinitnetwork += "  wlan0:\n"
            cloudinitnetwork += "    dhcp4: true\n"
            cloudinitnetwork += "    optional: true\n"
            cloudinitnetwork += "    access-points:\n"
            cloudinitnetwork += "      \""+fieldWifiSSID.text+"\":\n"
            cloudinitnetwork += "        password: \""+cryptedPsk+"\"\n"
            if (chkWifiSSIDHidden.checked) {
                cloudinitnetwork += "        hidden: true\n"
            }

            addCmdline("cfg80211.ieee80211_regdom="+fieldWifiCountry.editText)
        }

        if (firstrun.length) {
            firstrun = "#!/bin/bash\n\n"+"set +e\n\n"+firstrun
            addFirstRun("rm -f /boot/firstrun.sh")
            addFirstRun("sed -i 's| systemd.run.*||g' /boot/cmdline.txt")
            addFirstRun("exit 0")
            /* using systemd.run_success_action=none does not seem to have desired effect
               systemd then stays at "reached target kernel command line", so use reboot instead */
            //addCmdline("systemd.run=/boot/firstrun.sh systemd.run_success_action=reboot systemd.unit=kernel-command-line.target")
            // cmdline changing moved to DownloadThread::_customizeImage()
        }

        if (cloudinitwrite !== "") {
            addCloudInit("write_files:\n"+cloudinitwrite+"\n")
        }

        if (cloudinitrun !== "") {
            addCloudInit("runcmd:\n"+cloudinitrun+"\n")
        }

        imageWriter.setImageCustomization(config, cmdline, firstrun, cloudinit, cloudinitnetwork)
    }

    function saveSettings()
    {
        var settings = { };
        if (chkHostname.checked && fieldHostname.length) {
            settings.hostname = fieldHostname.text
        }

        if (chkWifi.checked) {
            settings.wifiSSID = fieldWifiSSID.text
            if (chkWifiSSIDHidden.checked) {
                settings.wifiSSIDHidden = true
            }
            settings.wifiCountry = fieldWifiCountry.editText

            const isPassphrase = fieldWifiPassword.text.length >= 8 &&
                fieldWifiPassword.text.length < 64
            var cryptedPsk = isPassphrase ? imageWriter.pbkdf2(fieldWifiPassword.text, fieldWifiSSID.text)
                                          : fieldWifiPassword.text
            settings.wifiPassword = cryptedPsk
        }

        imageWriter.setSetting("beep", chkBeep.checked)
        imageWriter.setSetting("eject", chkEject.checked)

        if (chkHostname.checked || chkWifi.checked) {
            /* OS customization to be applied. */
            hasSavedSettings = true
            saveSettingsSignal(settings)
        }
    }

    function clearCustomizationFields()
    {
        /* Bind copies of the lists */
        fieldWifiCountry.model = imageWriter.getCountryList()

        fieldHostname.text = "citadel"

        chkWifiSSIDHidden.checked = false
        chkHostname.checked = false

        /* WiFi Settings */
        fieldWifiSSID.text = imageWriter.getSSID()
        if (fieldWifiSSID.text.length) {
            fieldWifiPassword.text = imageWriter.getPSK()
            if (fieldWifiPassword.text.length) {
                chkShowPassword.checked = false
                if (Qt.platform.os == "osx") {
                    /* User indicated wifi must be prefilled */
                    chkWifi.checked = true
                }
            }
        }
    }
}
