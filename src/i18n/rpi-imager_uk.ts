<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>DownloadExtractThread</name>
    <message>
        <location filename="../downloadextractthread.cpp" line="196"/>
        <location filename="../downloadextractthread.cpp" line="385"/>
        <source>Error extracting archive: %1</source>
        <translation>Помилка розпакування архіва: %1</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="261"/>
        <source>Error mounting FAT32 partition</source>
        <translation>Помилка монтування FAT32 розділу</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="281"/>
        <source>Operating system did not mount FAT32 partition</source>
        <translation>Операційна система не монтувала FAT32 розділ</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="304"/>
        <source>Error changing to directory &apos;%1&apos;</source>
        <translation>Помилка при зміні каталогу на &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>DownloadThread</name>
    <message>
        <location filename="../downloadthread.cpp" line="118"/>
        <source>unmounting drive</source>
        <translation>диск від&apos;єднується</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="138"/>
        <source>opening drive</source>
        <translation>диск відкривається</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="166"/>
        <source>Error running diskpart: %1</source>
        <translation>Помилка при виконанні diskpart: %1</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="187"/>
        <source>Error removing existing partitions</source>
        <translation>Помилка при видаленні існуючих розділів</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="213"/>
        <source>Authentication cancelled</source>
        <translation>Аутентифікація скасована</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="216"/>
        <source>Error running authopen to gain access to disk device &apos;%1&apos;</source>
        <translation>Помилка виконання authopen для отримання доступу до пристроя &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="217"/>
        <source>Please verify if &apos;Citadel Imager&apos; is allowed access to &apos;removable volumes&apos; in privacy settings (under &apos;files and folders&apos; or alternatively give it &apos;full disk access&apos;).</source>
        <translation>Переконайтеся, що у Citadel Imager у налаштуваннях приватності (у розділі &quot;файли та каталоги&quot;) є доступ до змінних розділів. Або дайте програмі доступ до усього диску.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="239"/>
        <source>Cannot open storage device &apos;%1&apos;.</source>
        <translation>Не вдалося відкрити накопичувач &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="281"/>
        <source>discarding existing data on drive</source>
        <translation>видалення існуючих даних на диску</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="301"/>
        <source>zeroing out first and last MB of drive</source>
        <translation>обнулювання першого і останнього мегабайта диску</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="307"/>
        <source>Write error while zero&apos;ing out MBR</source>
        <translation>Помилка при обнулюванні MBR</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="319"/>
        <source>Write error while trying to zero out last part of card.&lt;br&gt;Card could be advertising wrong capacity (possible counterfeit).</source>
        <translation>Помилка запису під час обнулювання останнього розділу карти пам&apos;яті.&lt;br&gt;Можливо заявлений об&apos;єм карти не збігається з реальним (можливо карта є підробленою).</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="408"/>
        <source>starting download</source>
        <translation>початок завантаження</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="466"/>
        <source>Error downloading: %1</source>
        <translation>Помилка завантаження: %1</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="663"/>
        <source>Access denied error while writing file to disk.</source>
        <translation>Помилка доступу при записі файлу на диск.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="668"/>
        <source>Controlled Folder Access seems to be enabled. Please add both rpi-imager.exe and fat32format.exe to the list of allowed apps and try again.</source>
        <translation>Схоже, що увімкнено контрольований доступ до каталогу (Controlled Folder Access). Додайте rpi-imager.exe і fat32format.exe в список виключення та спробуйте ще раз.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="675"/>
        <source>Error writing file to disk</source>
        <translation>Помилка запису файлу на диск</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="697"/>
        <source>Download corrupt. Hash does not match</source>
        <translation>Завантаження пошкоджено. Хеш сума не збігається</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="709"/>
        <location filename="../downloadthread.cpp" line="761"/>
        <source>Error writing to storage (while flushing)</source>
        <translation>Помилка запису на накопичувач (при скидуванні)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="716"/>
        <location filename="../downloadthread.cpp" line="768"/>
        <source>Error writing to storage (while fsync)</source>
        <translation>Помилка запису на накопичувач (при виконанні fsync)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="751"/>
        <source>Error writing first block (partition table)</source>
        <translation>Помилка під час запису першого блоку (таблиця розділів)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="826"/>
        <source>Error reading from storage.&lt;br&gt;SD card may be broken.</source>
        <translation>Помилка читання накопичувача.&lt;br&gt;SD-карта пам&apos;яті може бути пошкоджена.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="845"/>
        <source>Verifying write failed. Contents of SD card is different from what was written to it.</source>
        <translation>Помилка перевірки запису. Зміст SD-карти пам&apos;яті відрізняється від того, що було записано туди.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="898"/>
        <source>Customizing image</source>
        <translation>Налаштування образа</translation>
    </message>
</context>
<context>
    <name>DriveFormatThread</name>
    <message>
        <location filename="../driveformatthread.cpp" line="63"/>
        <location filename="../driveformatthread.cpp" line="124"/>
        <location filename="../driveformatthread.cpp" line="185"/>
        <source>Error partitioning: %1</source>
        <translation>Помилка створення роздіу: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="84"/>
        <source>Error starting fat32format</source>
        <translation>Помилка запуску fat32format</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="94"/>
        <source>Error running fat32format: %1</source>
        <translation>Помилка під час виконання fat32format: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="104"/>
        <source>Error determining new drive letter</source>
        <translation>Помилка визначення  нової букви диску</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="109"/>
        <source>Invalid device: %1</source>
        <translation>Не правильний пристрій: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="146"/>
        <source>Error formatting (through udisks2)</source>
        <translation>Помилка форматування (через udisks2)</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="174"/>
        <source>Error starting sfdisk</source>
        <translation>Помилка запуску sfdisk</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="199"/>
        <source>Partitioning did not create expected FAT partition %1</source>
        <translation>При створенні розділів не було створено очікуваний розділ FAT %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="208"/>
        <source>Error starting mkfs.fat</source>
        <translation>Помилка запуску mkfs.fat</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="218"/>
        <source>Error running mkfs.fat: %1</source>
        <translation>Помилка виконання mkfs.fat: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="225"/>
        <source>Formatting not implemented for this platform</source>
        <translation>Форматування не доступно на цій платформі</translation>
    </message>
</context>
<context>
    <name>ImageWriter</name>
    <message>
        <location filename="../imagewriter.cpp" line="259"/>
        <source>Storage capacity is not large enough.&lt;br&gt;Needs to be at least %1 GB.</source>
        <translation>Місця на накопичувачі недостатньо.&lt;br&gt;Треба, щоб було хоча б %1 ГБ.</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="265"/>
        <source>Input file is not a valid disk image.&lt;br&gt;File size %1 bytes is not a multiple of 512 bytes.</source>
        <translation>Обраний файл не є правильним образом диску.&lt;br&gt;Розмір файла %1 байт не є кратним 512 байт.</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="596"/>
        <source>Erase</source>
        <translation type="unfinished">Видалити</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="597"/>
        <source>Format card as FAT32</source>
        <translation type="unfinished">Форматувати карту у FAT32</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="603"/>
        <source>Use custom</source>
        <translation type="unfinished">Власний образ</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="604"/>
        <source>Select a custom .img from your computer</source>
        <translation type="unfinished">Обрати власний .img з вашого комп&apos;ютера</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="654"/>
        <source>Downloading and writing image</source>
        <translation>Завантаження і запис образу</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="787"/>
        <source>Select image</source>
        <translation>Обрати образ</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="1085"/>
        <source>Would you like to prefill the wifi password from the system keychain?</source>
        <translation>Вказати пароль від Wi-Fi автоматично із системного ланцюга ключів?</translation>
    </message>
</context>
<context>
    <name>LocalFileExtractThread</name>
    <message>
        <location filename="../localfileextractthread.cpp" line="34"/>
        <source>opening image file</source>
        <translation>відкривання файлу образа</translation>
    </message>
    <message>
        <location filename="../localfileextractthread.cpp" line="39"/>
        <source>Error opening image file</source>
        <translation>Помилка при відкриванні файлу образу</translation>
    </message>
</context>
<context>
    <name>MsgPopup</name>
    <message>
        <location filename="../MsgPopup.qml" line="98"/>
        <source>NO</source>
        <translation>НІ</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="107"/>
        <source>YES</source>
        <translation>ТАК</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="116"/>
        <source>CONTINUE</source>
        <translation>ПРОДОВЖИТИ</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="124"/>
        <source>QUIT</source>
        <translation>ВИЙТИ</translation>
    </message>
</context>
<context>
    <name>OptionsPopup</name>
    <message>
        <location filename="../OptionsPopup.qml" line="20"/>
        <source>OS Customization</source>
        <translation>Налаштування ОС</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="62"/>
        <source>General</source>
        <translation>Загальні</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="65"/>
        <source>Options</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="81"/>
        <source>Set hostname:</source>
        <translation>Встановити ім&apos;я хосту:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="136"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="103"/>
        <source>Configure wireless LAN</source>
        <translation>Налаштувати бездротову LAN мережу</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="122"/>
        <source>SSID:</source>
        <translation>SSID:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="155"/>
        <source>Show password</source>
        <translation>Показати пароль</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="161"/>
        <source>Hidden SSID</source>
        <translation>Прихована SSID</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="167"/>
        <source>Wireless LAN country:</source>
        <translation>Країна бездротової LAN мережі:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="184"/>
        <source>Play sound when finished</source>
        <translation>Відтворити звук після завершення</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="188"/>
        <source>Eject media when finished</source>
        <translation>Витягнути накопичувач після завершення</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="202"/>
        <source>SAVE</source>
        <translation>ЗБЕРЕГТИ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../linux/linuxdrivelist.cpp" line="119"/>
        <source>Internal SD card reader</source>
        <translation>Внутрішній считувач SD карт</translation>
    </message>
</context>
<context>
    <name>UseSavedSettingsPopup</name>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="77"/>
        <source>Use OS customization?</source>
        <translation>Використовувати налаштування ОС?</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="92"/>
        <source>Would you like to apply OS customization settings?</source>
        <translation>Чи бажаєте ви прийняти налаштування ОС?</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="134"/>
        <source>NO</source>
        <translation>НІ</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="115"/>
        <source>NO, CLEAR SETTINGS</source>
        <translation>НІ, ОЧИСТИТИ НАЛАШТУВАННЯ</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="125"/>
        <source>YES</source>
        <translation>ТАК</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="102"/>
        <source>EDIT SETTINGS</source>
        <translation>РЕДАГУВАТИ НАЛАШТУВАННЯ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="117"/>
        <source>CHOOSE DEVICE</source>
        <translation>ОБРАТИ ПРИСТРІЙ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="143"/>
        <location filename="../main.qml" line="571"/>
        <source>Operating System</source>
        <translation>Операційна система</translation>
    </message>
    <message>
        <location filename="../main.qml" line="154"/>
        <location filename="../main.qml" line="1592"/>
        <source>CHOOSE OS</source>
        <translation>ОБРАТИ ОС</translation>
    </message>
    <message>
        <location filename="../main.qml" line="166"/>
        <source>Select this button to change the operating system</source>
        <translation>Натисніть на цю кнопку, щоб змінити операційну систему</translation>
    </message>
    <message>
        <location filename="../main.qml" line="217"/>
        <location filename="../main.qml" line="971"/>
        <source>Storage</source>
        <translation>Накопичувач</translation>
    </message>
    <message>
        <location filename="../main.qml" line="228"/>
        <location filename="../main.qml" line="1275"/>
        <source>CHOOSE STORAGE</source>
        <translation>ОБРАТИ НАКОПИЧУВАЧ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="242"/>
        <source>Select this button to change the destination storage device</source>
        <translation>Натисніть цю кнопку, щоб змінити пристрій призначення</translation>
    </message>
    <message>
        <location filename="../main.qml" line="288"/>
        <source>CANCEL WRITE</source>
        <translation>СКАСУВАТИ ЗАПИСУВАННЯ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="291"/>
        <location filename="../main.qml" line="1197"/>
        <source>Cancelling...</source>
        <translation>Скасування...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="303"/>
        <source>CANCEL VERIFY</source>
        <translation>СКАСУВАТИ ПЕРЕВІРКУ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="306"/>
        <location filename="../main.qml" line="1220"/>
        <location filename="../main.qml" line="1294"/>
        <source>Finalizing...</source>
        <translation>Завершення...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="315"/>
        <source>Next</source>
        <translation>Далі</translation>
    </message>
    <message>
        <location filename="../main.qml" line="321"/>
        <source>Select this button to start writing the image</source>
        <translation>Натисніть цю кнопку, щоб розпочати запис образу</translation>
    </message>
    <message>
        <location filename="../main.qml" line="343"/>
        <source>Using custom repository: %1</source>
        <translation>Користуючись власним репозиторієм: %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="352"/>
        <source>Keyboard navigation: &lt;tab&gt; navigate to next button &lt;space&gt; press button/select item &lt;arrow up/down&gt; go up/down in lists</source>
        <translation>Навігація клавіатурою: клавіша &lt;Tab&gt; переміститися на наступну кнопку, клавіша &lt;Пробіл&gt; натиснути кнопку/обрати елемент, клавіши з &lt;стрілками вниз/вгору&gt; переміститися вниз/вгору по списку</translation>
    </message>
    <message>
        <location filename="../main.qml" line="373"/>
        <source>Language: </source>
        <translation>Мова: </translation>
    </message>
    <message>
        <location filename="../main.qml" line="396"/>
        <source>Keyboard: </source>
        <translation>Клавіатура: </translation>
    </message>
    <message>
        <location filename="../main.qml" line="22"/>
        <source>Citadel Imager v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="105"/>
        <location filename="../main.qml" line="468"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="129"/>
        <source>Select this button to choose your target device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="180"/>
        <source>Customizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="191"/>
        <source>CUSTOMIZE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="203"/>
        <source>Select this button to customize additional settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="487"/>
        <source>[ All ]</source>
        <translation>[Усі]</translation>
    </message>
    <message>
        <location filename="../main.qml" line="641"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../main.qml" line="642"/>
        <source>Go back to main menu</source>
        <translation>Повернутися у головне меню</translation>
    </message>
    <message>
        <location filename="../main.qml" line="884"/>
        <source>Released: %1</source>
        <translation>Випущено: %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="894"/>
        <source>Cached on your computer</source>
        <translation>Кешовано на вашому комп&apos;ютері</translation>
    </message>
    <message>
        <location filename="../main.qml" line="896"/>
        <source>Local file</source>
        <translation>Локальний файл</translation>
    </message>
    <message>
        <location filename="../main.qml" line="898"/>
        <source>Online - %1 GB download</source>
        <translation>Онлайн - потрібно завантажити %1 ГБ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="899"/>
        <source>Online - %1 MB download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="1024"/>
        <location filename="../main.qml" line="1076"/>
        <location filename="../main.qml" line="1082"/>
        <source>Mounted as %1</source>
        <translation>Примонтовано як %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1078"/>
        <source>[WRITE PROTECTED]</source>
        <translation>ЗАХИЩЕНО ВІД ЗАПИСУ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1123"/>
        <source>Are you sure you want to quit?</source>
        <translation>Бажаєте вийти?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1124"/>
        <source>Citadel Imager is still busy.&lt;br&gt;Are you sure you want to quit?</source>
        <translation>Citadel Imager все ще зайнятий.&lt;br&gt;Ви впевнені, що бажаєте вийти?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1135"/>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1144"/>
        <source>Preparing to write...</source>
        <translation>Підготовка до запису...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1159"/>
        <source>All existing data on &apos;%1&apos; will be erased.&lt;br&gt;Are you sure you want to continue?</source>
        <translation>Усі уснуючі дані у &apos;%1&apos; будуть видалені.&lt;br&gt; Ви впевнені, що бажаєте продовжити?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1174"/>
        <source>Update available</source>
        <translation>Доступно оновлення</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1175"/>
        <source>There is a newer version of Imager available.&lt;br&gt;Would you like to visit the website to download it?</source>
        <translation>Доступна нова версія Imager.&lt;br&gt;Бажаєте завітати на сайт та завантажити її?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1200"/>
        <source>Writing... %1%</source>
        <translation>Записування...%1%</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1223"/>
        <source>Verifying... %1%</source>
        <translation>Перевірка...%1%</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1230"/>
        <source>Preparing to write... (%1)</source>
        <translation>Підготовка до запису... (%1)</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1251"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1258"/>
        <source>Write Successful</source>
        <translation>Успішно записано</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1259"/>
        <source>Erase</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1260"/>
        <source>&lt;b&gt;%1&lt;/b&gt; has been erased&lt;br&gt;&lt;br&gt;You can now remove the SD card from the reader</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; був успішно видалено.br&gt;&lt;br&gt; тепер можна дістати SD карту із считувача</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1267"/>
        <source>&lt;b&gt;%1&lt;/b&gt; has been written to &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;&lt;br&gt;You can now remove the SD card from the reader</source>
        <translation>Записування &lt;b&gt;%1&lt;/b&gt; на &lt;b&gt;%2&lt;/b&gt; виконано &lt;br&gt;&lt;br&gt; Тепер можна дістати SD карту із считувача</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1417"/>
        <source>Error parsing os_list.json</source>
        <translation>Помилка парсування os_list.json</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1666"/>
        <source>Connect an USB stick containing images first.&lt;br&gt;The images must be located in the root folder of the USB stick.</source>
        <translation>Спочатку під&apos;єднайте USB-накопичувач з образами.&lt;br&gt;Образи повинні знаходитися у корінному каталогу USB-накопичувача.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1682"/>
        <source>SD card is write protected.&lt;br&gt;Push the lock switch on the left side of the card upwards, and try again.</source>
        <translation>SD карта захищена від запису.&lt;br&gt;перетягніть перемикач на лівій стороні картки вгору, і спробуйте ще раз.</translation>
    </message>
</context>
</TS>
