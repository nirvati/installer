<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>DownloadExtractThread</name>
    <message>
        <location filename="../downloadextractthread.cpp" line="196"/>
        <location filename="../downloadextractthread.cpp" line="385"/>
        <source>Error extracting archive: %1</source>
        <translation>Arşiv çıkarılırken hata oluştu: %1</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="261"/>
        <source>Error mounting FAT32 partition</source>
        <translation>FAT32 bölümü bağlanırken hata oluştu</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="281"/>
        <source>Operating system did not mount FAT32 partition</source>
        <translation>İşletim sistemi FAT32 bölümünü bağlamadı</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="304"/>
        <source>Error changing to directory &apos;%1&apos;</source>
        <translation>Dizin değiştirirken hata oluştu &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>DownloadThread</name>
    <message>
        <location filename="../downloadthread.cpp" line="118"/>
        <source>unmounting drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="138"/>
        <source>opening drive</source>
        <translation>sürücü açılıyor</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="166"/>
        <source>Error running diskpart: %1</source>
        <translation>Diskpart çalıştırılırken hata oluştu: %1</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="187"/>
        <source>Error removing existing partitions</source>
        <translation>Mevcut bölümler kaldırılırken hata oluştu </translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="213"/>
        <source>Authentication cancelled</source>
        <translation>Kimlik doğrulama iptal edildi</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="216"/>
        <source>Error running authopen to gain access to disk device &apos;%1&apos;</source>
        <translation>&apos;%1&apos; disk aygıtına erişmek için authopen çalıştırılırken hata oluştu</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="217"/>
        <source>Please verify if &apos;Citadel Imager&apos; is allowed access to &apos;removable volumes&apos; in privacy settings (under &apos;files and folders&apos; or alternatively give it &apos;full disk access&apos;).</source>
        <translation>Lütfen &apos;Citadel Imager&apos;ın gizlilik ayarlarında (&apos;dosyalar ve klasörler&apos; altında veya alternatif olarak &apos;tam disk erişimi&apos;) &apos;çıkarılabilir birimlere erişim&apos; izin verilip verilmediğini doğrulayın.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="239"/>
        <source>Cannot open storage device &apos;%1&apos;.</source>
        <translation>Depolama cihazı açılamıyor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="281"/>
        <source>discarding existing data on drive</source>
        <translation>sürücüdeki mevcut verileri sil</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="301"/>
        <source>zeroing out first and last MB of drive</source>
        <translation>sürücünün ilk ve son MB&apos;sini sıfırlama</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="307"/>
        <source>Write error while zero&apos;ing out MBR</source>
        <translation>MBR sıfırlanırken yazma hatası</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="319"/>
        <source>Write error while trying to zero out last part of card.&lt;br&gt;Card could be advertising wrong capacity (possible counterfeit).</source>
        <translation>Kartın son kısmını sıfırlamaya çalışırken yazma hatası. Kart yanlış kapasitenin tanımını yapıyor olabilir (olası sahte bölüm boyutu tanımı)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="408"/>
        <source>starting download</source>
        <translation>indirmeye başlanıyor</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="466"/>
        <source>Error downloading: %1</source>
        <translation>İndirilirken hata oluştu: %1</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="663"/>
        <source>Access denied error while writing file to disk.</source>
        <translation>Dosyayı diske yazarken erişim reddedildi hatası</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="668"/>
        <source>Controlled Folder Access seems to be enabled. Please add both rpi-imager.exe and fat32format.exe to the list of allowed apps and try again.</source>
        <translation>Kontrollü Klasör Erişimi etkin görünüyor. Lütfen izin verilen uygulamalar listesine hem rpi-imager.exe&apos;yi hem de fat32format.exe&apos;yi ekleyin ve tekrar deneyin.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="675"/>
        <source>Error writing file to disk</source>
        <translation>Dosyayı diske yazma hatası</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="697"/>
        <source>Download corrupt. Hash does not match</source>
        <translation>İndirme bozuk. Hash eşleşmiyor</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="709"/>
        <location filename="../downloadthread.cpp" line="761"/>
        <source>Error writing to storage (while flushing)</source>
        <translation>Depolama alanına yazma hatası (flushing sırasında)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="716"/>
        <location filename="../downloadthread.cpp" line="768"/>
        <source>Error writing to storage (while fsync)</source>
        <translation>Depoya yazma hatası (fsync sırasında)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="751"/>
        <source>Error writing first block (partition table)</source>
        <translation>İlk bloğu yazma hatası (bölüm tablosu)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="826"/>
        <source>Error reading from storage.&lt;br&gt;SD card may be broken.</source>
        <translation>Depolamadan okuma hatası.&lt;br&gt;SD kart arızalı olabilir.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="845"/>
        <source>Verifying write failed. Contents of SD card is different from what was written to it.</source>
        <translation>Yazma doğrulanamadı. SD kartın içeriği, üzerine yazılandan farklı.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="898"/>
        <source>Customizing image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DriveFormatThread</name>
    <message>
        <location filename="../driveformatthread.cpp" line="63"/>
        <location filename="../driveformatthread.cpp" line="124"/>
        <location filename="../driveformatthread.cpp" line="185"/>
        <source>Error partitioning: %1</source>
        <translation>Bölümleme hatası: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="84"/>
        <source>Error starting fat32format</source>
        <translation>fat32format başlatılırken hata oluştu</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="94"/>
        <source>Error running fat32format: %1</source>
        <translation>fat32format çalıştırılırken hata oluştu: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="104"/>
        <source>Error determining new drive letter</source>
        <translation>Yeni sürücü harfi belirlenirken hata oluştu</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="109"/>
        <source>Invalid device: %1</source>
        <translation>Geçersiz cihaz: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="146"/>
        <source>Error formatting (through udisks2)</source>
        <translation>Hatalı biçimlendirme (udisks2 aracılığıyla)</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="174"/>
        <source>Error starting sfdisk</source>
        <translation>sfdisk başlatılırken hata oluştu</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="199"/>
        <source>Partitioning did not create expected FAT partition %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="208"/>
        <source>Error starting mkfs.fat</source>
        <translation>mkfs.fat başlatılırken hata oluştu</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="218"/>
        <source>Error running mkfs.fat: %1</source>
        <translation>mkfs.fat çalıştırılırken hata oluştu: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="225"/>
        <source>Formatting not implemented for this platform</source>
        <translation>Bu platform için biçimlendirme uygulanmadı</translation>
    </message>
</context>
<context>
    <name>ImageWriter</name>
    <message>
        <location filename="../imagewriter.cpp" line="259"/>
        <source>Storage capacity is not large enough.&lt;br&gt;Needs to be at least %1 GB.</source>
        <translation>Depolama kapasitesi yeterince büyük değil.&lt;br&gt;En az %1 GB olması gerekiyor</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="265"/>
        <source>Input file is not a valid disk image.&lt;br&gt;File size %1 bytes is not a multiple of 512 bytes.</source>
        <translation>Giriş dosyası geçerli bir disk görüntüsü değil.&lt;br&gt;%1 bayt dosya boyutu 512 baytın katı değil.</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="596"/>
        <source>Erase</source>
        <translation type="unfinished">Sil</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="597"/>
        <source>Format card as FAT32</source>
        <translation type="unfinished">Kartı FAT32 olarak biçimlendir</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="603"/>
        <source>Use custom</source>
        <translation type="unfinished">Özel imaj kullan</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="604"/>
        <source>Select a custom .img from your computer</source>
        <translation type="unfinished">Bilgisayarınızdan özel bir .img seçin</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="654"/>
        <source>Downloading and writing image</source>
        <translation>Görüntü indirme ve yazma</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="787"/>
        <source>Select image</source>
        <translation>Imaj seç</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="1085"/>
        <source>Would you like to prefill the wifi password from the system keychain?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocalFileExtractThread</name>
    <message>
        <location filename="../localfileextractthread.cpp" line="34"/>
        <source>opening image file</source>
        <translation>imaj dosyası açılıyor</translation>
    </message>
    <message>
        <location filename="../localfileextractthread.cpp" line="39"/>
        <source>Error opening image file</source>
        <translation>Imaj dosyası açılırken hata oluştu</translation>
    </message>
</context>
<context>
    <name>MsgPopup</name>
    <message>
        <location filename="../MsgPopup.qml" line="98"/>
        <source>NO</source>
        <translation>HAYIR</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="107"/>
        <source>YES</source>
        <translation>EVET</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="116"/>
        <source>CONTINUE</source>
        <translation>DEVAM ET</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="124"/>
        <source>QUIT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsPopup</name>
    <message>
        <location filename="../OptionsPopup.qml" line="20"/>
        <source>OS Customization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="62"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="65"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="81"/>
        <source>Set hostname:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="136"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="103"/>
        <source>Configure wireless LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="122"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="155"/>
        <source>Show password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="161"/>
        <source>Hidden SSID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="167"/>
        <source>Wireless LAN country:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="184"/>
        <source>Play sound when finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="188"/>
        <source>Eject media when finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="202"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../linux/linuxdrivelist.cpp" line="119"/>
        <source>Internal SD card reader</source>
        <translation>Dahili SD kart okuyucu</translation>
    </message>
</context>
<context>
    <name>UseSavedSettingsPopup</name>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="77"/>
        <source>Use OS customization?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="92"/>
        <source>Would you like to apply OS customization settings?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="134"/>
        <source>NO</source>
        <translation>HAYIR</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="115"/>
        <source>NO, CLEAR SETTINGS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="125"/>
        <source>YES</source>
        <translation type="unfinished">EVET</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="102"/>
        <source>EDIT SETTINGS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="117"/>
        <source>CHOOSE DEVICE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="143"/>
        <location filename="../main.qml" line="571"/>
        <source>Operating System</source>
        <translation>İşletim sistemi</translation>
    </message>
    <message>
        <location filename="../main.qml" line="154"/>
        <location filename="../main.qml" line="1592"/>
        <source>CHOOSE OS</source>
        <translation>İŞLETİM SİSTEMİ SEÇİN</translation>
    </message>
    <message>
        <location filename="../main.qml" line="166"/>
        <source>Select this button to change the operating system</source>
        <translation>İşletim sistemini değiştirmek için bu düğmeyi seçin</translation>
    </message>
    <message>
        <location filename="../main.qml" line="217"/>
        <location filename="../main.qml" line="971"/>
        <source>Storage</source>
        <translation>SD Kart</translation>
    </message>
    <message>
        <location filename="../main.qml" line="228"/>
        <location filename="../main.qml" line="1275"/>
        <source>CHOOSE STORAGE</source>
        <translation>SD KART SEÇİN</translation>
    </message>
    <message>
        <location filename="../main.qml" line="242"/>
        <source>Select this button to change the destination storage device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="288"/>
        <source>CANCEL WRITE</source>
        <translation>YAZMAYI İPTAL ET</translation>
    </message>
    <message>
        <location filename="../main.qml" line="291"/>
        <location filename="../main.qml" line="1197"/>
        <source>Cancelling...</source>
        <translation>İptal ediliyor...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="303"/>
        <source>CANCEL VERIFY</source>
        <translation>DOĞRULAMA İPTALİ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="306"/>
        <location filename="../main.qml" line="1220"/>
        <location filename="../main.qml" line="1294"/>
        <source>Finalizing...</source>
        <translation>Bitiriliyor...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="315"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="321"/>
        <source>Select this button to start writing the image</source>
        <translation>Görüntüyü yazmaya başlamak için bu düğmeyi seçin</translation>
    </message>
    <message>
        <location filename="../main.qml" line="343"/>
        <source>Using custom repository: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="352"/>
        <source>Keyboard navigation: &lt;tab&gt; navigate to next button &lt;space&gt; press button/select item &lt;arrow up/down&gt; go up/down in lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="373"/>
        <source>Language: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="396"/>
        <source>Keyboard: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="22"/>
        <source>Citadel Imager v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="105"/>
        <location filename="../main.qml" line="468"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="129"/>
        <source>Select this button to choose your target device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="180"/>
        <source>Customizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="191"/>
        <source>CUSTOMIZE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="203"/>
        <source>Select this button to customize additional settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="487"/>
        <source>[ All ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="641"/>
        <source>Back</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="../main.qml" line="642"/>
        <source>Go back to main menu</source>
        <translation>Ana menüye dön</translation>
    </message>
    <message>
        <location filename="../main.qml" line="884"/>
        <source>Released: %1</source>
        <translation>Yayın: %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="894"/>
        <source>Cached on your computer</source>
        <translation>Bilgisayarınızda önbelleğe alındı
</translation>
    </message>
    <message>
        <location filename="../main.qml" line="896"/>
        <source>Local file</source>
        <translation>Yerel dosya</translation>
    </message>
    <message>
        <location filename="../main.qml" line="898"/>
        <source>Online - %1 GB download</source>
        <translation>Çevrimiçi -%1 GB indir</translation>
    </message>
    <message>
        <location filename="../main.qml" line="899"/>
        <source>Online - %1 MB download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="1024"/>
        <location filename="../main.qml" line="1076"/>
        <location filename="../main.qml" line="1082"/>
        <source>Mounted as %1</source>
        <translation>%1 olarak bağlandı.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1078"/>
        <source>[WRITE PROTECTED]</source>
        <translation>[YAZMA KORUMALI]</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1123"/>
        <source>Are you sure you want to quit?</source>
        <translation>Çıkmak istediğine emin misin?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1124"/>
        <source>Citadel Imager is still busy.&lt;br&gt;Are you sure you want to quit?</source>
        <translation>Citadel Imager hala meşgul.&lt;br&gt;Çıkmak istediğinizden emin misiniz?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1135"/>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1144"/>
        <source>Preparing to write...</source>
        <translation>Yazdırmaya hazırlanıyor...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1159"/>
        <source>All existing data on &apos;%1&apos; will be erased.&lt;br&gt;Are you sure you want to continue?</source>
        <translation>&apos;%1&apos; üzerindeki mevcut tüm veriler silinecek.&lt;br&gt;Devam etmek istediğinizden emin misiniz?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1174"/>
        <source>Update available</source>
        <translation>Güncelleme bulunuyor</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1175"/>
        <source>There is a newer version of Imager available.&lt;br&gt;Would you like to visit the website to download it?</source>
        <translation>Görüntüleyicinin daha yeni bir sürümü var. &lt;br&gt; İndirmek için web sitesini ziyaret etmek ister misiniz?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1200"/>
        <source>Writing... %1%</source>
        <translation>Yazılıyor... %1%</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1223"/>
        <source>Verifying... %1%</source>
        <translation>Doğrulanıyor... %1%</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1230"/>
        <source>Preparing to write... (%1)</source>
        <translation>Yazdırmaya hazırlanıyor... (%1)</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1251"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1258"/>
        <source>Write Successful</source>
        <translation>Başarılı Yazıldı</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1259"/>
        <source>Erase</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1260"/>
        <source>&lt;b&gt;%1&lt;/b&gt; has been erased&lt;br&gt;&lt;br&gt;You can now remove the SD card from the reader</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; silindi &lt;br&gt;&lt;br&gt; Artık SD kartı okuyucudan çıkarabilirsiniz</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1267"/>
        <source>&lt;b&gt;%1&lt;/b&gt; has been written to &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;&lt;br&gt;You can now remove the SD card from the reader</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;&lt;br&gt; üzerine yazıldı. Artık SD kartı okuyucudan çıkarabilirsiniz</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1417"/>
        <source>Error parsing os_list.json</source>
        <translation>os_list.json ayrıştırma hatası</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1666"/>
        <source>Connect an USB stick containing images first.&lt;br&gt;The images must be located in the root folder of the USB stick.</source>
        <translation>Önce görüntüler içeren bir USB bellek bağlayın.&lt;br&gt; Görüntüler USB belleğin kök klasöründe bulunmalıdır.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1682"/>
        <source>SD card is write protected.&lt;br&gt;Push the lock switch on the left side of the card upwards, and try again.</source>
        <translation>SD kart yazma korumalı. &lt;br&gt; Kartın sol tarafındaki kilit anahtarını yukarı itin ve tekrar deneyin.</translation>
    </message>
</context>
</TS>
