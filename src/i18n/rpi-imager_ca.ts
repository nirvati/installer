<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>DownloadExtractThread</name>
    <message>
        <location filename="../downloadextractthread.cpp" line="196"/>
        <location filename="../downloadextractthread.cpp" line="385"/>
        <source>Error extracting archive: %1</source>
        <translation>S&apos;ha produït un error en extreure l&apos;arxiu: %1</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="261"/>
        <source>Error mounting FAT32 partition</source>
        <translation>S&apos;ha produït un error en muntar la partició FAT32</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="281"/>
        <source>Operating system did not mount FAT32 partition</source>
        <translation>El sistema operatiu no ha muntat la partició FAT32</translation>
    </message>
    <message>
        <location filename="../downloadextractthread.cpp" line="304"/>
        <source>Error changing to directory &apos;%1&apos;</source>
        <translation>S&apos;ha produït un error en canviar al directori «%1»</translation>
    </message>
</context>
<context>
    <name>DownloadThread</name>
    <message>
        <location filename="../downloadthread.cpp" line="118"/>
        <source>unmounting drive</source>
        <translation>S&apos;està desmuntant el dispositiu</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="138"/>
        <source>opening drive</source>
        <translation>S&apos;està obrint la unitat</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="166"/>
        <source>Error running diskpart: %1</source>
        <translation>S&apos;ha produït un error en executar «diskpart»: %1</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="187"/>
        <source>Error removing existing partitions</source>
        <translation>S&apos;ha produït un error en eliminar les particions existents.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="213"/>
        <source>Authentication cancelled</source>
        <translation>S&apos;ha cancel·lat l&apos;autenticació</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="216"/>
        <source>Error running authopen to gain access to disk device &apos;%1&apos;</source>
        <translation>S&apos;ha produït un error en executar «authopen» per a obtenir l&apos;accés al dispositiu de disc «%1»</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="217"/>
        <source>Please verify if &apos;Citadel Imager&apos; is allowed access to &apos;removable volumes&apos; in privacy settings (under &apos;files and folders&apos; or alternatively give it &apos;full disk access&apos;).</source>
        <translation>Verifiqueu si el «Citadel Imager» té accés als «volums extraïbles» des de la configuració de privacitat (sota «fitxers i carpetes» o doneu-li «accés complet al disc»)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="239"/>
        <source>Cannot open storage device &apos;%1&apos;.</source>
        <translation>No s&apos;ha pogut obrir el dispositiu d&apos;emmagatzematge «%1»</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="281"/>
        <source>discarding existing data on drive</source>
        <translation>S&apos;estan descartant les dades existents a la unitat</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="301"/>
        <source>zeroing out first and last MB of drive</source>
        <translation>S&apos;està esborrant amb zeros el primer i l&apos;últim MB de la unitat</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="307"/>
        <source>Write error while zero&apos;ing out MBR</source>
        <translation>S&apos;ha produït un error en esborrar amb zeros l&apos;«MBR».</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="319"/>
        <source>Write error while trying to zero out last part of card.&lt;br&gt;Card could be advertising wrong capacity (possible counterfeit).</source>
        <translation>S&apos;ha produït un error d&apos;escriptura en esborrar amb zeros l&apos;última part de la targeta.&lt;br&gt;La targeta podria estar indicant una capacitat errònia (possible falsificació)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="408"/>
        <source>starting download</source>
        <translation>S&apos;està iniciant la baixada</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="466"/>
        <source>Error downloading: %1</source>
        <translation>S&apos;ha produït un error en la baixada: %1</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="663"/>
        <source>Access denied error while writing file to disk.</source>
        <translation>S&apos;ha produït un error d&apos;accés denegat en escriure el fitxer al disc.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="668"/>
        <source>Controlled Folder Access seems to be enabled. Please add both rpi-imager.exe and fat32format.exe to the list of allowed apps and try again.</source>
        <translation>L&apos;opció «Controla l&apos;accés de la carpeta» de la Seguretat del Windows sembla que està activada. Afegiu els executables «rpi-imager.exe» i «fat32format.exe» a la llista d&apos;aplicacions permeses i torneu-ho a provar.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="675"/>
        <source>Error writing file to disk</source>
        <translation>S&apos;ha produït un error en escriure el fitxer al disc</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="697"/>
        <source>Download corrupt. Hash does not match</source>
        <translation>La baixada està corrompuda. El «hash» no coincideix</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="709"/>
        <location filename="../downloadthread.cpp" line="761"/>
        <source>Error writing to storage (while flushing)</source>
        <translation>S&apos;ha produït un error en escriure a l&apos;emmagatzematge (procés: Flushing)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="716"/>
        <location filename="../downloadthread.cpp" line="768"/>
        <source>Error writing to storage (while fsync)</source>
        <translation>S&apos;ha produït un error en escriure a l&apos;emmagatzematge (procés: fsync)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="751"/>
        <source>Error writing first block (partition table)</source>
        <translation>S&apos;ha produït un error en escriure el primer bloc (taula de particions)</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="826"/>
        <source>Error reading from storage.&lt;br&gt;SD card may be broken.</source>
        <translation>S&apos;ha produït un error en llegir l&apos;emmagatzematge.&lt;br&gt;És possible que la targeta SD estigui malmesa.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="845"/>
        <source>Verifying write failed. Contents of SD card is different from what was written to it.</source>
        <translation>Ha fallat la verificació de l&apos;escriptura. El contingut de la targeta SD és diferent del que s&apos;hi ha escrit.</translation>
    </message>
    <message>
        <location filename="../downloadthread.cpp" line="898"/>
        <source>Customizing image</source>
        <translation>S&apos;està personalitzant la imatge</translation>
    </message>
</context>
<context>
    <name>DriveFormatThread</name>
    <message>
        <location filename="../driveformatthread.cpp" line="63"/>
        <location filename="../driveformatthread.cpp" line="124"/>
        <location filename="../driveformatthread.cpp" line="185"/>
        <source>Error partitioning: %1</source>
        <translation>S&apos;ha produït un error durant la partició: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="84"/>
        <source>Error starting fat32format</source>
        <translation>S&apos;ha produït un error en iniciar «fat32format»</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="94"/>
        <source>Error running fat32format: %1</source>
        <translation>S&apos;ha produït un error en executar «fat32format»: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="104"/>
        <source>Error determining new drive letter</source>
        <translation>S&apos;ha produït un error en determinar la lletra de la nova unitat</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="109"/>
        <source>Invalid device: %1</source>
        <translation>El dispositiu no és vàlid: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="146"/>
        <source>Error formatting (through udisks2)</source>
        <translation>S&apos;ha produït un error en formatar (a través d&apos;«udisks2»)</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="174"/>
        <source>Error starting sfdisk</source>
        <translation>S&apos;ha produït un error en iniciar «sfdisk»</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="199"/>
        <source>Partitioning did not create expected FAT partition %1</source>
        <translation>No s&apos;ha pogut crear la partició FAT %1 esperada</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="208"/>
        <source>Error starting mkfs.fat</source>
        <translation>S&apos;ha produït un error en iniciar «mkfs.fat»</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="218"/>
        <source>Error running mkfs.fat: %1</source>
        <translation>S&apos;ha produït un error en executar «mkfs.fat»: %1</translation>
    </message>
    <message>
        <location filename="../driveformatthread.cpp" line="225"/>
        <source>Formatting not implemented for this platform</source>
        <translation>Aquesta plataforma no té implementada la formatació</translation>
    </message>
</context>
<context>
    <name>ImageWriter</name>
    <message>
        <location filename="../imagewriter.cpp" line="259"/>
        <source>Storage capacity is not large enough.&lt;br&gt;Needs to be at least %1 GB.</source>
        <translation>La capacitat de l&apos;emmagatzematge no és suficient.&lt;br&gt;Ha de ser de %1 GB com a mínim.</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="265"/>
        <source>Input file is not a valid disk image.&lt;br&gt;File size %1 bytes is not a multiple of 512 bytes.</source>
        <translation>El fitxer d&apos;entrada no és una imatge de disc vàlida.&lt;br&gt;La mida del fitxer és de %1 bytes, que no és múltiple de 512 bytes.</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="596"/>
        <source>Erase</source>
        <translation type="unfinished">Esborra</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="597"/>
        <source>Format card as FAT32</source>
        <translation type="unfinished">Formata la targeta com a FAT32</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="603"/>
        <source>Use custom</source>
        <translation type="unfinished">Utilitza una personalitzada</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="604"/>
        <source>Select a custom .img from your computer</source>
        <translation type="unfinished">Selecciona una imatge .img personalitzada de l&apos;ordinador</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="654"/>
        <source>Downloading and writing image</source>
        <translation>S&apos;està baixant i escrivint la imatge</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="787"/>
        <source>Select image</source>
        <translation>Selecciona una imatge</translation>
    </message>
    <message>
        <location filename="../imagewriter.cpp" line="1085"/>
        <source>Would you like to prefill the wifi password from the system keychain?</source>
        <translation>Voleu emplenar la contrasenya del wifi des del clauer del sistema?</translation>
    </message>
</context>
<context>
    <name>LocalFileExtractThread</name>
    <message>
        <location filename="../localfileextractthread.cpp" line="34"/>
        <source>opening image file</source>
        <translation>S&apos;està obrint el fitxer de la imatge</translation>
    </message>
    <message>
        <location filename="../localfileextractthread.cpp" line="39"/>
        <source>Error opening image file</source>
        <translation>S&apos;ha produït un error en obrir el fitxer de la imatge</translation>
    </message>
</context>
<context>
    <name>MsgPopup</name>
    <message>
        <location filename="../MsgPopup.qml" line="98"/>
        <source>NO</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="107"/>
        <source>YES</source>
        <translation>SÍ</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="116"/>
        <source>CONTINUE</source>
        <translation>CONTINUA</translation>
    </message>
    <message>
        <location filename="../MsgPopup.qml" line="124"/>
        <source>QUIT</source>
        <translation>SURT</translation>
    </message>
</context>
<context>
    <name>OptionsPopup</name>
    <message>
        <location filename="../OptionsPopup.qml" line="20"/>
        <source>OS Customization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="62"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="65"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="81"/>
        <source>Set hostname:</source>
        <translation>Defineix un nom de la màquina (hostname):</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="136"/>
        <source>Password:</source>
        <translation>Contrasenya:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="103"/>
        <source>Configure wireless LAN</source>
        <translation>Configura la wifi</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="122"/>
        <source>SSID:</source>
        <translation>SSID:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="155"/>
        <source>Show password</source>
        <translation>Mostra la contrasenya</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="161"/>
        <source>Hidden SSID</source>
        <translation>SSID oculta</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="167"/>
        <source>Wireless LAN country:</source>
        <translation>País del wifi:</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="184"/>
        <source>Play sound when finished</source>
        <translation>Fes un so quan acabi</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="188"/>
        <source>Eject media when finished</source>
        <translation>Expulsa el mitjà quan acabi</translation>
    </message>
    <message>
        <location filename="../OptionsPopup.qml" line="202"/>
        <source>SAVE</source>
        <translation>DESA</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../linux/linuxdrivelist.cpp" line="119"/>
        <source>Internal SD card reader</source>
        <translation>Lector de targetes SD intern</translation>
    </message>
</context>
<context>
    <name>UseSavedSettingsPopup</name>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="77"/>
        <source>Use OS customization?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="92"/>
        <source>Would you like to apply OS customization settings?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="134"/>
        <source>NO</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="115"/>
        <source>NO, CLEAR SETTINGS</source>
        <translation>NO, ESBORRA LA CONFIGURACIÓ</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="125"/>
        <source>YES</source>
        <translation>SÍ</translation>
    </message>
    <message>
        <location filename="../UseSavedSettingsPopup.qml" line="102"/>
        <source>EDIT SETTINGS</source>
        <translation>EDITA LA CONFIGURACIÓ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="117"/>
        <source>CHOOSE DEVICE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="143"/>
        <location filename="../main.qml" line="571"/>
        <source>Operating System</source>
        <translation>Sistema operatiu</translation>
    </message>
    <message>
        <location filename="../main.qml" line="154"/>
        <location filename="../main.qml" line="1592"/>
        <source>CHOOSE OS</source>
        <translation>ESCULL SO</translation>
    </message>
    <message>
        <location filename="../main.qml" line="166"/>
        <source>Select this button to change the operating system</source>
        <translation>Seleccioneu aquest botó si voleu canviar el sistema operatiu</translation>
    </message>
    <message>
        <location filename="../main.qml" line="217"/>
        <location filename="../main.qml" line="971"/>
        <source>Storage</source>
        <translation>Emmagatzematge</translation>
    </message>
    <message>
        <location filename="../main.qml" line="228"/>
        <location filename="../main.qml" line="1275"/>
        <source>CHOOSE STORAGE</source>
        <translation>ESCULL L&apos;EMMAGATZEMATGE</translation>
    </message>
    <message>
        <location filename="../main.qml" line="242"/>
        <source>Select this button to change the destination storage device</source>
        <translation>Seleccioneu aquest botó per a canviar la destinació del dispositiu d&apos;emmagatzematge</translation>
    </message>
    <message>
        <location filename="../main.qml" line="288"/>
        <source>CANCEL WRITE</source>
        <translation>CANCEL·LA L&apos;ESCRIPTURA</translation>
    </message>
    <message>
        <location filename="../main.qml" line="291"/>
        <location filename="../main.qml" line="1197"/>
        <source>Cancelling...</source>
        <translation>S&apos;està cancel·lant...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="303"/>
        <source>CANCEL VERIFY</source>
        <translation>CANCEL·LA LA VERIFICACIÓ</translation>
    </message>
    <message>
        <location filename="../main.qml" line="306"/>
        <location filename="../main.qml" line="1220"/>
        <location filename="../main.qml" line="1294"/>
        <source>Finalizing...</source>
        <translation>S&apos;està finalitzant...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="315"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="321"/>
        <source>Select this button to start writing the image</source>
        <translation>Seleccioneu aquest botó per a començar l&apos;escriptura de la imatge</translation>
    </message>
    <message>
        <location filename="../main.qml" line="343"/>
        <source>Using custom repository: %1</source>
        <translation>S&apos;està usant el repositori personalitzat: %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="352"/>
        <source>Keyboard navigation: &lt;tab&gt; navigate to next button &lt;space&gt; press button/select item &lt;arrow up/down&gt; go up/down in lists</source>
        <translation>Navegació per teclat: &lt;tab&gt; navega al botó següent &lt;espai&gt; prem el botó o selecciona l&apos;element &lt;fletxa amunt o avall&gt; desplaçament per les llistes</translation>
    </message>
    <message>
        <location filename="../main.qml" line="373"/>
        <source>Language: </source>
        <translation>Idioma: </translation>
    </message>
    <message>
        <location filename="../main.qml" line="396"/>
        <source>Keyboard: </source>
        <translation>Teclat: </translation>
    </message>
    <message>
        <location filename="../main.qml" line="22"/>
        <source>Citadel Imager v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="105"/>
        <location filename="../main.qml" line="468"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="129"/>
        <source>Select this button to choose your target device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="180"/>
        <source>Customizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="191"/>
        <source>CUSTOMIZE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="203"/>
        <source>Select this button to customize additional settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="487"/>
        <source>[ All ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="641"/>
        <source>Back</source>
        <translation>Enrere</translation>
    </message>
    <message>
        <location filename="../main.qml" line="642"/>
        <source>Go back to main menu</source>
        <translation>Torna al menú principal</translation>
    </message>
    <message>
        <location filename="../main.qml" line="884"/>
        <source>Released: %1</source>
        <translation>Llançat el: %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="894"/>
        <source>Cached on your computer</source>
        <translation>A la memòria cau de l&apos;ordinador</translation>
    </message>
    <message>
        <location filename="../main.qml" line="896"/>
        <source>Local file</source>
        <translation>Fitxer local</translation>
    </message>
    <message>
        <location filename="../main.qml" line="898"/>
        <source>Online - %1 GB download</source>
        <translation>Disponible en línia (%1 GB)</translation>
    </message>
    <message>
        <location filename="../main.qml" line="899"/>
        <source>Online - %1 MB download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="1024"/>
        <location filename="../main.qml" line="1076"/>
        <location filename="../main.qml" line="1082"/>
        <source>Mounted as %1</source>
        <translation>Muntat com a %1</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1078"/>
        <source>[WRITE PROTECTED]</source>
        <translation>[PROTEGIT CONTRA ESCRIPTURA]</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1123"/>
        <source>Are you sure you want to quit?</source>
        <translation>N&apos;esteu segur que voleu sortir?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1124"/>
        <source>Citadel Imager is still busy.&lt;br&gt;Are you sure you want to quit?</source>
        <translation>El Citadel Imager està ocupat.&lt;br&gt;N&apos;esteu segur que voleu sortir?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1135"/>
        <source>Warning</source>
        <translation>Avís</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1144"/>
        <source>Preparing to write...</source>
        <translation>S&apos;està preparant per a escriure...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1159"/>
        <source>All existing data on &apos;%1&apos; will be erased.&lt;br&gt;Are you sure you want to continue?</source>
        <translation>Totes les dades existents a «%1» s&apos;esborraràn.&lt;br&gt;Esteu segur que voleu continuar?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1174"/>
        <source>Update available</source>
        <translation>Hi ha una actualització disponible</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1175"/>
        <source>There is a newer version of Imager available.&lt;br&gt;Would you like to visit the website to download it?</source>
        <translation>Hi ha una nova versió de l&apos;Imager disponible.&lt;br&gt;Voleu visitar el lloc web per baixar-la?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1200"/>
        <source>Writing... %1%</source>
        <translation>S&apos;està escrivint... %1%</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1223"/>
        <source>Verifying... %1%</source>
        <translation>S&apos;està verificant... %1%</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1230"/>
        <source>Preparing to write... (%1)</source>
        <translation>S&apos;està preparant per escriure... (%1)</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1251"/>
        <source>Error</source>
        <translation>S&apos;ha produït un error</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1258"/>
        <source>Write Successful</source>
        <translation>S&apos;ha escrit amb èxit</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1259"/>
        <source>Erase</source>
        <translation>Esborra</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1260"/>
        <source>&lt;b&gt;%1&lt;/b&gt; has been erased&lt;br&gt;&lt;br&gt;You can now remove the SD card from the reader</source>
        <translation>S&apos;ha esborrat &lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ja podeu retirar la targeta SD del lector</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1267"/>
        <source>&lt;b&gt;%1&lt;/b&gt; has been written to &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;&lt;br&gt;You can now remove the SD card from the reader</source>
        <translation>S&apos;ha escrit el «&lt;b&gt;%1&lt;/b&gt;» a &lt;b&gt;%2&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ja podeu retirar la targeta SD del lector</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1417"/>
        <source>Error parsing os_list.json</source>
        <translation>S&apos;ha produït un error en analitzar os_lists.json</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1666"/>
        <source>Connect an USB stick containing images first.&lt;br&gt;The images must be located in the root folder of the USB stick.</source>
        <translation>Connecteu una memòria USB que contingui primer imatges.&lt;br&gt;Les imatges s&apos;han de trobar a la carpeta arrel de la memòria.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="1682"/>
        <source>SD card is write protected.&lt;br&gt;Push the lock switch on the left side of the card upwards, and try again.</source>
        <translation>La targeta SD està protegida contra escriptura.&lt;br&gt;Accioneu l&apos;interruptor del costat esquerre de la targeta SD per tal que quedi posicionat a la part superior i torneu-ho a provar.</translation>
    </message>
</context>
</TS>
